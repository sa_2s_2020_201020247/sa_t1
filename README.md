# PRACTICA 1

_Aplicación elaboradara en .Net Core en su versión 3.1, se  trata de una aplicación de consola en la cual se trabajó con REST para la obtención y envío de datos_

## Información General
- Practica 1 Laboratorio Software Avanzado
- Creado por Haroldo Arias
- Carnet 201020247
- Agosto 2020

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Instalación 🔧

_La instalación es muy sencilla, si tienes visual studio en su versión 2019 solo basta con abrirla con el IDE y ejecutarla, caso contrario te explicó el procedimiento_

### Pre-requisitos 📋
_Debes de tener instalado el SDK del .NetCore en su versión 3.1_
* [.Net Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) - Acá puedes descargarlo

## Compilación ⚙️
_Si se utiliza el IDE del Visual Studio solo basta con Compilarlo dentro del IDE_
Manualmente hay que correr el siguiente Comenzando
```
dotnet build
``` 

## Ejecución del Programa
_Si se utiliza el IDE del Visual Studio solo basta con Ejecutarlo dentro del IDE_
Manualmente hay que correr el siguiente Comenzando
```
dotnet run --project=SA_T1_201020247
``` 

## Autor ✒️

* **Haroldo Pablo Arias Molina** - *Trabajo Inicial* - [harias25](https://github.com/harias25)
 
## Licencia 📄

Este proyecto está bajo la Licencia Libre - mira el archivo [LICENSE.md](LICENSE.md) para detalles