﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SA_T1_201020247
{
    /// <summary>
    /// Clase que se utiliza tanto para el parseo de las listas obtenidas, como para obtener el resultado del POST en el momento de la creación
    /// </summary>
    class Registro
    {
        public int id { get; set; }
        public String name { get; set; }
        public Boolean result { get; set; }
    }
}
