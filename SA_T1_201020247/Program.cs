﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace SA_T1_201020247
{
    /// <summary>
    /// Clase principal de la Aplicacion
    /// </summary>
    class Program
    {
        static Utilidades utilidades;

        /// <summary>
        /// Metodo inicial de la aplicación
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            
            utilidades = new Utilidades();
            int opcion = -1;
            while(opcion != 0)
            {
                try
                {
                    printMenu();
                    String value = Console.ReadLine();
                    bool success = Int32.TryParse(value, out opcion);
                    if (success)
                    {
                        switch (opcion)
                        {
                            case 0: break;
                            case 1:
                                getAll();
                                break;
                            case 2:
                                Console.Write("Ingrese un número de Carnet: ");
                                value = Console.ReadLine();
                                getWithCarnet(value.Replace(" ",""));
                                break;
                            case 3:
                                Console.Write("Ingrese el Id: ");
                                int id;
                                value = Console.ReadLine();
                                success = Int32.TryParse(value, out id);
                                if (success)
                                {
                                    getWithId(id);
                                }
                                else
                                {
                                    Console.WriteLine("Solo se permiten ingresar números!!");
                                    Console.ReadLine();
                                }
                                break;
                            case 4:
                                Console.Write("Ingrese el Nombre del Registro a Ingresar: ");
                                value = Console.ReadLine();
                                newItem(value);
                                break;
                            default:
                                Console.Write("Opción Incorrecta!!!");
                                Console.ReadLine();
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Solo se permiten ingresar números!!");
                        Console.ReadLine();
                        opcion = -1;
                    }
                }
                catch
                {
                    Console.WriteLine("Ocurrió un problema en el sistema!!");
                    Console.ReadLine();
                    opcion = -1;
                }
            }
        }

        /// <summary>
        /// Método estatico para imprimir el menú de la aplicación
        /// </summary>
        private static void printMenu()
        {
            Console.Clear();
            Console.WriteLine("************** TAREA 1 LAB Software Avanzado **************");
            Console.WriteLine("**************** Haroldo Pablo Arias Molina ***************");
            Console.WriteLine("Opciones de la Aplicación: ");
            Console.WriteLine("0) Salir de la Aplicación");
            Console.WriteLine("1) Listar Todos los Registros");
            Console.WriteLine("2) Filtrar Registros por Carnet");
            Console.WriteLine("3) Buscar Registro por su Id");
            Console.WriteLine("4) Ingresar nuevo Registro");
            Console.Write("Seleccione una opción del menú: ");
        }

        /// <summary>
        /// Método estatico que tiene como función principal la cración de los registros enviando una petición POST al cliente, si este se crea imprime el Id generado del registro segun el resultado del Cliente
        /// </summary>
        /// <param name="name">Valor que se debe de Enviar para crear el Registro</param>
        private static void newItem(String name)
        {
            try
            {
                String resultado = utilidades.executeAPI("POST",
                                                    "https://api.softwareavanzado.world/index.php?webserviceClient=administrator&webserviceVersion=1.0.0&option=contact&api=hal",
                                                    "{\"name\":\"" + name + "\"}");
                Registro registro = JsonConvert.DeserializeObject<Registro>(resultado);
                Console.Clear();

                if (registro != null)
                {
                    if (registro.result)
                    {
                        Console.WriteLine("Se ha insertado exitosamente el registro con el Id: " + registro.id);
                    }
                    else
                    {
                        Console.WriteLine("No se ha podido insertar el Registro");
                    }

                }
                else
                {
                    Console.WriteLine("No se ha podido insertar el Registro");
                }
            }
            catch
            {
                Console.WriteLine("Ocurrio un problema insertando el registro = " + name + "!!");
            }

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Presione Enter para regresar al menú anterior......");
            Console.ReadLine();
        }

        /// <summary>
        /// Método que tiene como función principal imprimir un listado de Registros
        /// </summary>
        /// <param name="registros">Lista de tipo Registro que se desea imprimir</param>
        private static void printList(List<Registro> registros)
        {
            Console.Clear();
            if (registros != null)
            {
                Console.WriteLine("Id | Name");
                foreach (Registro r in registros)
                {
                    Console.WriteLine(r.id + " | " + r.name);
                }
            }
            else
            {
                Console.WriteLine("No se encontraron registros!!");
            }
            
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Presione Enter para regresar al menú anterior......");
        }

        /// <summary>
        /// Método que realiza una petición GET al cliente enviando un parametro con el filtro a realizar, en este caso se envia el carnet ingresado, sin embargo tomará cualquier valor para hacer el filtro
        /// </summary>
        /// <param name="carnet">Valor que se enviar para realizar el filtro</param>
        private static void getWithCarnet(String carnet)
        {
            try
            {
                String contenido = utilidades.executeAPI("GET",
                                                    "https://api.softwareavanzado.world/index.php?option=com_contact&webserviceVersion=1.0.0&webserviceClient=administrator&filter[search]=" + carnet + "&api=Hal",
                                                    null);

                if (contenido != "")
                {
                    var parsed = JObject.Parse(contenido);
                    List<Registro> registros = new List<Registro>();
                    var listado = parsed.SelectToken("_embedded");
                    listado = JObject.Parse(listado.ToString());
                    listado = listado.SelectToken("item");
                    registros = JsonConvert.DeserializeObject<List<Registro>>(listado.ToString());
                    printList(registros);
                }
                else
                {
                    printList(null);
                }
            }
            catch
            {
                Console.WriteLine("Ocurrio un problema filtrando los registros con el carnet = " + carnet + "!!");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("Presione Enter para regresar al menú anterior......");
            }
            
            
            Console.ReadLine();
        }
        /// <summary>
        /// Método que realiza una petición GET al cliente enviando el parametro del Id que se desea buscar, si se tiene resultado se imprime en pantalla el resultado obtenido
        /// </summary>
        /// <param name="Id">Llave que se desea buscar</param>
        private static void getWithId(int Id)
        {
            try
            {
                String contenido = utilidades.executeAPI("GET",
                                                    "https://api.softwareavanzado.world/index.php?option=com_contact&webserviceVersion=1.0.0&webserviceClient=administrator&id=" + Id + "&api=Hal",
                                                    null);

                Registro registro = JsonConvert.DeserializeObject<Registro>(contenido);
                Console.Clear();

                if (registro != null)
                {
                    Console.WriteLine("Id: " + registro.id);
                    Console.WriteLine("Name: " + registro.name);
                }
                else
                {
                    Console.WriteLine("El Id --> " + Id + ", no existe!!");
                }
            }
            catch
            {
                Console.WriteLine("Ocurrio un problema obteniendo registro con id = "+Id+"!!");
            }
            
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Presione Enter para regresar al menú anterior......");
            Console.ReadLine();

        }
        /// <summary>
        /// Método que realiza una petición GET al cliente para obtener todos los registros existentes y posterior a esto imprimirlos en pantalla
        /// </summary>
        private static void getAll()
        {
            try
            {
                String contenido = utilidades.executeAPI("GET",
                                                    "https://api.softwareavanzado.world/index.php?option=com_contact&webserviceVersion=1.0.0&webserviceClient=administrator&list[limit]=10000&api=Hal",
                                                    null);

                if (contenido != "")
                {
                    var parsed = JObject.Parse(contenido);
                    List<Registro> registros = new List<Registro>();
                    var listado = parsed.SelectToken("_embedded");
                    listado = JObject.Parse(listado.ToString());
                    listado = listado.SelectToken("item");
                    registros = JsonConvert.DeserializeObject<List<Registro>>(listado.ToString());
                    printList(registros);
                }
                else
                {
                    printList(null);
                }
                
            }
            catch
            {
                Console.WriteLine("Ocurrio un problema obteniendo el listado de registros!!");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("Presione Enter para regresar al menú anterior......");
            }
            Console.ReadLine();
        }

    }
}
